<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $companies = \App\Company::orderBy('id', 'desc')/*->where('status', '=', 'enabled')*/->take(10)->get();
    return view('welcome', ["companies" => $companies]);
});

Route::get('/infopage', function () {
    return view('infopage');
});

Route::get('/wepay', function () {
    return view('wepay');
});


Auth::routes();
Route::get('/home', 'HomeController@index');
Route::get('/add/new/company', 'HomeController@addNewCompany');
Route::get('/edit/company/{slug}', 'HomeController@editCompany');
Route::get('/my/settings', 'HomeController@userSettings');
Route::post('/ajax/change/user/email', 'HomeController@changeUserEmail');
Route::post('/ajax/change/user/settings', 'HomeController@changeUserSettings');


/*Route::post('/stripe/charge', 'StripeController@charge');*/
Route::get('/stripe', 'StripeController@index');
Route::get('/get/stripes', 'StripeController@getStripeChargeData');
Route::get('/stripe/create/an/account', 'StripeController@createAnAccountApi');
Route::get('/stripe/retrieve/account/details', 'StripeController@retrieveAccountDetails');
Route::get('/stripe/retrieve/balance/{key}', 'StripeController@retrieveBalance');
Route::get('/stripe/creating/destination', 'StripeController@creatingDestinationCharges');
Route::get('/stripe/retrive/charge/{slug}', 'StripeController@retrieveCharge');
Route::get('/stripe/retrive/list/customers', 'StripeController@getListCustomers');
Route::get('/stripe/create/payout', 'StripeController@createPayout');
Route::get('/stripe/authentication', 'StripeController@authentication');
Route::get('/stripe/connected/accounts', 'StripeController@listAllConnectedAccounts');
Route::get('/stripe/create/bank/account', 'StripeController@createBankAccount');
Route::get('/stripe/create/bank/account/token', 'StripeController@createBankAccountToken');
Route::get('/stripe/delete/account/{id}', 'StripeController@deleteAccount');

Route::post('/paymethod', 'PayController@payMethod');
Route::get('/payform/{slug}', 'PayController@index');

/*AdminController*/
Route::get('/admin', 'AdminController@index');
Route::get('/admin/companies', 'AdminController@companies');
Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/donations', 'AdminController@donations');
Route::get('/admin/payments', 'AdminController@payments');
Route::get('/admin/checkouts', 'AdminController@checkouts');
Route::get('/admin/options', 'AdminController@options');
Route::get('/admin/wepay_users', 'AdminController@wepayUsers');
Route::get('/admin/docs', 'AdminController@docs');
Route::get('/admin/stripe_accounts', 'AdminController@stripeAccounts');
Route::get('/admin/stripe_charges', 'AdminController@stripeCharges');
Route::get('/admin/api', 'AdminController@api');
Route::get('/admin/user/balance/{id}', 'AdminController@getUserBalance');
Route::post('/admin/add/option', 'AdminController@addOption');
Route::post('/admin/update/option', 'AdminController@updateOption');
Route::post('/admin/remove/option', 'AdminController@removeOption');
Route::post('/admin/ajax/company/change/status', 'AdminController@changeCompanyStatus');

Route::get('/admin/categories', 'AdminController@companyCategories');
Route::post('/admin/add/category', 'AdminController@addCategory');
Route::post('/admin/update/category', 'AdminController@updateCategory');
Route::post('/admin/remove/category', 'AdminController@removeCategory');


Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('login/google', 'Auth\LoginController@redirectToProviderGoogle');
Route::get('callback/google', 'Auth\LoginController@handleProviderCallbackGoogle');

/*CompaniesController*/
Route::get('/company/{slug}', 'CompaniesController@company');
Route::post('/remove/company', 'CompaniesController@removeCompany');
Route::post('/create/new/company', 'CompaniesController@createCompany');
Route::post('/update/company', 'CompaniesController@updateCompany');
Route::post('/remove/company/image', 'CompaniesController@removeImage');
Route::post('/add/comment/to/company', 'CompaniesController@addComment');

/*DonatesController*/
Route::get('/donete/for/{slug}', 'DonatesController@donateFor');
Route::any('/donate/step2/', 'DonatesController@donateStep2');
Route::get('/balance/recalculation', 'DonatesController@balanceReCalculationAll'); //!
Route::post('/stripe/pay', 'DonatesController@stripePay');
/*End DonatesController*/

/*WepayController*/
Route::get('/checkout/info', 'WepayController@checkoutInfo');
Route::get('/checkouts/info', 'WepayController@checkoutsInfo'); //!
Route::get('/custome/account/creation', 'WepayController@customAccountCreation'); //!
Route::get('/payment/account/info', 'WepayController@paymentAccountInfo'); //!
Route::get('/payment/accounts/info', 'WepayController@paymentAccountsInfo'); //!
Route::get('/withdrawals', 'WepayController@withdrawals'); //!
Route::post('/ajax/wepay/credit_card/create', 'WepayController@creditCardCreate');
Route::post('/ajax/wepay/custome/checkout/create', 'WepayController@makePay');
/*End WepayController*/

/*OptionsController*/
Route::get('/success/page', 'OptionsController@success');
Route::post('/set/country', 'OptionsController@setCountry');

Route::get('socialAuth/{type}', 'SocialConnectController@getSocialLogin');
Route::get('callback', 'SocialConnectController@getSocialProcess');
Route::get('logout', 'SocialConnectController@getLogout');









