<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WepayCheckout extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'checkout_id',
        'account_id',
        'type',
        'short_description',
        'currency',
        'amount',
        'state',
        'gross',
        'payer_email',
        'payer_name',
        'reference_id',
        'user_pay_id',
        'balance_included'
    ];

    /**
     * @param $checkoutInfo
     * @param $companyId
     * @param $userId
     */
    public function createCheckoutWepay($checkoutInfo, $companyId, $userId)
    {
        WepayCheckout::create([
            'checkout_id' => $checkoutInfo->checkout_id,
            'account_id' => $checkoutInfo->account_id,
            'reference_id' => $companyId,
            'user_pay_id' => $userId,
            'type' => $checkoutInfo->type,
            'short_description' => $checkoutInfo->short_description,
            'currency' => $checkoutInfo->currency,
            'amount' => $checkoutInfo->amount,
            'state' => $checkoutInfo->state,
            'gross' => $checkoutInfo->gross,
            'payer_email' => $checkoutInfo->payer->email,
            'payer_name' => $checkoutInfo->payer->name,
        ]);
    }
}
