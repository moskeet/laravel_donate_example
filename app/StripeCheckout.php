<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripeCheckout extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'checkout_id',
        'amount',
        'destination',
        'status',
        'user_id',
        'company_id',
        'transfer',
        'balance_included'
    ];

    /**
     * @param $result
     * @param $userId
     * @param $companyId
     */
    public function stripeCheckoutSave($result, $userId, $companyId)
    {
        StripeCheckout::create([
            'checkout_id' => $result->id,
            'amount' => $result->amount,
            'destination' => $result->destination,
            'status' => $result->status,
            'user_id' => $userId,
            'transfer' => $result->transfer,
            'company_id' => $companyId,
        ]);
    }
}






