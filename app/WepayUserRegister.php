<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WepayUserRegister extends Model
{

    protected $table = "wepay_users_register";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'user_local_id',
        'access_token',
        'token_type',
        'expires_in',
        'email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
