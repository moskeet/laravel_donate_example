<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Donate extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'amount',
        'status',
        'company_id',
        'first_name',
        'last_name',
        'email',
        'country',
        'zip',
        'comment',
        'user_id',
        'anonymous'
    ];

    public function createDotate($checkoutInfo, $companyId, $userId, $anonymous, $isStripe = null) {
        if($userId == ""){
            $userId = null;
        }

        if($isStripe) {
            $amount =  $checkoutInfo->amount/100;
            $status = $checkoutInfo->status;
            $email = $_REQUEST['email'];
            $first_name = $_REQUEST['first_name'];
            $description = $_REQUEST['comment'];
        } else {
            $amount =  $checkoutInfo->amount;
            $status = $checkoutInfo->state;
            $email = $checkoutInfo->payer->email;
            $first_name = $checkoutInfo->payer->name;
            $description =  $checkoutInfo->short_description;
        }

        Donate::create([
            'company_id' => $companyId,
            'description' => $description,
            'amount' => $amount,
            'status' => $status,
            'email' => $email,
            'first_name' => $first_name,
            'user_id' => $userId,
            'anonymous' => $anonymous //donate anonymous or not
        ]);
    }
}

