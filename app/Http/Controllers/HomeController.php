<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $companies = Company::where("user_id", "=", $userId)/*->where('status', '=', 'enabled')*/->get();
        return view('home', ['companies' => $companies]);
    }

    public function addNewCompany()
    {
        $categories = Categories::all();
        return view('add_company', ['categories' => $categories]);
    }

    public function editCompany($slug)
    {
        $userId = Auth::user()->id;
        $company = Company::where('slug', '=', $slug)->where('status', '=', 'enabled')->where('user_id', '=', $userId)->first();
        $categories = Categories::all();
        return view('add_company', ['company' => $company, 'categories' => $categories]);
    }

    /**
     * user settings
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userSettings()
    {
        $user = Auth::user();
        return view('user_settings', ['user' => $user]);
    }

    public function changeUserEmail(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|unique:users|email|max:255',
        ]);

        $userId = Auth::user()->id;

        /*if(Auth::user()->pay_account_type == "stripe") {
            $res = DB::select("select sa.account_id, sa.secret from users as u join stripe_account_register as sa where u.id = $userId and u.id = sa.user_local_id");
            $stripeConterolle = new StripeController();
            $dataArray['email'] = $request->email;
            $result = $stripeConterolle->updateAnAccount($res[0]->account_id, $res[0]->secret, $dataArray);
        }*/

        $user = User::find($userId);
        $user->email = $request->email;
        $user->save();

        return response()->json(['status' => 'complete']);
    }

    public function changeUserSettings(Request $request) {
        $user = User::find(Auth::user()->id);
        $user[$request->row] = $request->value;
        $user->save();

        return response()->json(['status' => 'complete']);
    }

}
