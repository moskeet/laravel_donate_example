<?php

namespace App\Http\Controllers;

use App\StripeAccountRegister;
use App\StripeCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class StripeController extends Controller
{

    public $appKey = "sk_test_TzhejHF2kMweiTFs8S4EJPo9";
    public $commision = 5;

    public static function index($request = null)
    {
        if (isset($_GET['amount'])) {
            $amount = $_GET['amount'];
        } else {
            $amount = $request->amount;
        }
        return view('stripe', ["amount" => $amount,]);
    }

   /* public function charge()
    {
        $token = $_POST['stripeToken'];
        $stripeEmail = $_POST['stripeEmail'];
        $amount = $_POST['amount'];
        $customer = \Stripe\Customer::create(array(
            'email' => $stripeEmail,
            'source' => $token
        ));

        if (Auth::check()) {
            $userId = Auth::user()->id;
        } else {
            $userId = "";
        }

        $charge = \Stripe\Charge::create(array(
            'customer' => $customer->id,
            'amount' => $amount,
            'currency' => 'usd',
            'description' => $userId,
        ));
        $amount = $amount / 100;
        echo '<h1>Successfully charged ' . $amount . '!</h1>';
    }*/

    /**
     * api
     * get charge list
     */
    public function getStripeChargeData()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/charges?limit=3");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }

    /**
     * api
     * Create Stripe accounts for your users
     */
    public function createAnAccountApi($managed, $email, $country)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/accounts");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "managed=$managed&country=$country&email=$email"); //managed ??
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");
        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result);

        if (isset($result->error)) {
            echo $result->error->message;
            exit;
        } else {
            return $result;
        }
    }

    public function createAnAccount($managed, $email, $country, $localUserId)
    {
        $apiAccount = $this->createAnAccountApi($managed, $email, $country);
        StripeAccountRegister::create([
            'account_id' => $apiAccount->id,
            'user_local_id' => $localUserId,
            'name' => $apiAccount->display_name,
            'country' => $apiAccount->country,
            'email' => $apiAccount->email,
            'secret' => $apiAccount->keys->secret,
            'publishable' => $apiAccount->keys->publishable,
        ]);
    }

    /**
     * api
     * Retrieve account details
     */
    public function retrieveAccountDetails($account)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/accounts/$account");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");
        $result = curl_exec($ch);
        return $result;
    }

    /**
     * api
     * Update an account
     * @param $account
     */
    public function updateAnAccount($account, $secret, $dataArray) {

        $dataStr = "";
        foreach ($dataArray as $key => $data){
            $dataStr = $dataStr."$key=$data&";
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/accounts/$account");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "managed=true");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");

        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        echo $result; exit;
        curl_close ($ch);
        return $result;
    }

    /**
     * api
     * Retrieves the current account balance.
     */
    public function retrieveBalance($key)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/balance");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_USERPWD, $key . ":" . "");
        $result = curl_exec($ch);
        return $result;
    }

    /**
     * api
     */
    public function creatingDestinationCharges($card, $expMonth, $expYear, $cvc, $amount, $accountId, $email, $saveCustomer, $selectedCustomerId)  //createCustomer
    {

        //Можно создать Customer или каждый раз использовать новый токен. ....
        // $resultTocken = $this->cardToken($card, $expMonth, $expYear, $cvc, $accountId);
        // source=$resultTocken->id вместо customer=$resultTocken->id

        /**
         * Если Customer выбран из списка сохраненных
         */
        if(!isset($selectedCustomerId)){

            $resultTocken = $this->createCustomer($card, $expMonth, $expYear, $cvc, $email);

            if(isset($resultTocken->error)) {
                return $resultTocken;
            }
            if($saveCustomer == 1) {
                $stripeCustomer = new StripeCustomer();
                $stripeCustomer->stripeCustomerSave($email, $resultTocken->id, $card);
            }
            $selectedCustomerId = $resultTocken->id;
        }

        $amount = $amount * 100; //conver to cents
        $amountComission = ($amount - ($amount/100 * $this->commision)); // commision 5 %
        $amountComission = $amount - $amountComission;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/charges");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "amount=$amount&currency=usd&customer=$selectedCustomerId&destination[account]=$accountId&application_fee=$amountComission"); //source //&destination[amount]=$amountComission
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");
        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $result = json_decode($result);

        return $result;
    }

    /**
     * api
     * Retrieve a payout
     */
   /* public function retrievePayout() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/payouts/tr_1AJjoQEnMK94RSJQNMZWcm7H");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");
        $result = curl_exec($ch);
        return $result;
    }*/


   public function authentication() {
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/charges");
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
       curl_setopt($ch, CURLOPT_USERPWD, "sk_test_TzhejHF2kMweiTFs8S4EJPo9" . ":" . "");
       $result = curl_exec($ch);
       return $result;
   }

    /**
     *
     */
   public function createPayout() {
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/payouts");
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_POSTFIELDS, "amount=400&currency=usd");
       curl_setopt($ch, CURLOPT_POST, 1);
       curl_setopt($ch, CURLOPT_USERPWD, "sk_test_7ufvyAK5ooeJSPw13KLDrfou" . ":" . "");
       $headers = array();
       $headers[] = "Content-Type: application/x-www-form-urlencoded";
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       $result = curl_exec($ch);
       return $result;
   }

    /**
     * api
     * Retrieve a charge
     */
    public function retrieveCharge($slug) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/charges/$slug");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");
        $result = curl_exec($ch);
        return $result;
    }

    /**
     * When you create a new credit card,
     * you must specify a customer or recipient to create it on.
     */
    public function createCard() {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/customers/cus_AezmlHGDKtYSMY/sources");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "source=tok_1AIIGeEnMK94RSJQ67UZC6ZB");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "sk_test_TzhejHF2kMweiTFs8S4EJPo9" . ":" . "");

        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
    }


    /**
     * Creates a new customer object.
     */
    public function createCustomer($card, $expMonth, $expYear, $cvc, $email) {
        $token = $this->cardToken($card, $expMonth, $expYear, $cvc);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/customers");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "description=Customer&source=$token->id&email=$email");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");

        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        return \GuzzleHttp\json_decode($result);
    }

    public function getListCustomers() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/customers?limit=20");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_USERPWD, $this->appKey . ":" . "");
        $result = curl_exec($ch);
        curl_close ($ch);
        return $result;
    }

    /**
     * @param $card
     * @param $expMonth
     * @param $expYear
     * @param $cvc
     * @return mixed
     */
    public function cardToken($card, $expMonth, $expYear, $cvc, $accountId = null)
    {

        /*if(isset($accountId)){
            $stripeAccount = StripeAccountRegister::where('account_id', '=', $accountId)->first();
            $sk = $stripeAccount->secret;
        } else {
            $sk = $this->appKey;
        }*/

        $sk = $this->appKey;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/tokens");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "card[number]=$card&card[exp_month]=$expMonth&card[exp_year]=$expYear&card[cvc]=$cvc");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $sk . ":" . "");
        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $result = json_decode($result);
        return $result;
    }

    /**
     * List all connected accounts
     */
    public function listAllConnectedAccounts() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/accounts?limit=300");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_USERPWD, "sk_test_TzhejHF2kMweiTFs8S4EJPo9" . ":" . "");
        $result = curl_exec($ch);
        return $result;
        curl_close ($ch);
    }

    //Create a bank account token
    public function createBankAccountToken() {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/tokens");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "bank_account[country]=US&bank_account[currency]=usd&bank_account[account_holder_name]=Ella&bank_account[account_holder_type]=individual&bank_account[routing_number]=110000000&bank_account[account_number]=000123456789");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "sk_test_7ufvyAK5ooeJSPw13KLDrfou" . ":" . "");

        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        return \GuzzleHttp\json_decode($result)->id;
    }

    /**
     * api
     * @return mixed
     */
    public function createBankAccount() {

        $token = $this->createBankAccountToken();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/accounts/acct_1AKod1FdZxHG5vhU/external_accounts");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "external_account=$token");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "sk_test_7ufvyAK5ooeJSPw13KLDrfou" . ":" . "");

        $headers = array();
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        return $result;
        curl_close ($ch);
    }

    //Delete an account
    public function deleteAccount($accountId) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/accounts/$accountId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_USERPWD, "sk_test_TzhejHF2kMweiTFs8S4EJPo9" . ":" . "");
        $result = curl_exec($ch);
        return $result;
    }

    /*public function createCreditCard($card, $expMonth, $expYear, $cvc) {
        $res = $this->cardToken($card, $expMonth, $expYear, $cvc);
        $token = $res->id;
        $this->createCard();
    }*/



}
