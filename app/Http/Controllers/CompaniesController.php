<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Company;
use App\Donate;
use App\Http\Requests\StoreComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CompaniesController extends Controller
{

    public function index()
    {
        $userId = Auth::user()->id;
        $companies = Company::where("user_id", "=", $userId)->get();
        return view('home', ['companies' => $companies]);
    }


    public function company($slug)
    {
        $company = Company::where("slug", "=", $slug)/*->where('status', '=', 'enabled')*/
        ->first();
        $comments = Comment::where('company_id', '=', $company->id)->get();
        $donates = Donate::where('company_id', '=', $company->id)->get();
        return view('company', ['company' => $company, 'comments' => $comments, 'donates' => $donates]);
    }

    /**
     * remove company
     * @param Request $request
     * @return mixed
     */
    public function removeCompany(Request $request) {
        Company::where('id', '=', $request->id)->where('user_id', '=', Auth::user()->id)->delete();
        return response()->json(['status' => 'complete']);
    }

    /**
     * create company
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createCompany(Request $request) {
        list($validationFails, $validator) = $this->companyValidation($request);
        if($validationFails) {
            return redirect('/add/new/company')
                ->withErrors($validator)
                ->withInput();
        }
        $company = new Company();
        $company = $company->createCompany($request);
        return redirect('/company/'.$company->slug)->with('message', 'Complete');
    }

    /**
     * @param Request $request
     */
    public function updateCompany(Request $request) {
        list($validationFails, $validator) = $this->companyValidation($request);
        if($validationFails) {
            return redirect('/add/new/company')
                ->withErrors($validator)
                ->withInput();
        }
        $company = new Company();
        $company->editCompany($request);
        return redirect()->back()->with('message', 'Complete');
    }

    public function companyValidation($request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'amount_need' => 'required|integer',
        ]);
        return [$validator->fails(), $validator];
    }

    public function removeImage(Request $request) {
        $keyImg = $request->key;
        $companyId = $request->company_id;
        $company = Company::where("id", '=', $companyId)->where("user_id", '=', Auth::user()->id)->first();
        $companyimages = \GuzzleHttp\json_decode($company->images, true);
        foreach ($companyimages as $key => $image) {
            if($image == $keyImg){
                unset($companyimages[$key]);
                $url = public_path()."/thumbnail/$keyImg";
                if(file_exists($url)) {
                    unlink($url);
                }
                $company->images = \GuzzleHttp\json_encode($companyimages);
                $company->save();
                break;
            }
        }
        return response()->json(['status' => 'complete']);
    }

    public function addComment(StoreComment $request) {

        if(!Auth::check()){
            return redirect()->back();
        }

        Comment::create([
            'text' => $request->text,
            'user_id' => Auth::user()->id,
            'company_id' => $request->id,
            'position' => 0
        ]);

        return redirect()->back();

    }

}
