<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class PayController extends Controller
{
    public function index(Request $request, $slug) {
        $company = Company::where("slug", "=", $slug)->first();
        if(!isset($company)){
            echo "company not exist";
            exit;
        }
        if(isset($request->methodPay)){
            return view("payform", ["methodPay" => $request->methodPay, "company" => $company]);
        } else {
            return view("payform", ["methodPay" => null, "company" => $company]);
        }
    }

    public function payMethod(Request $request)
    {
        if ($request->methodPay == "wepay") {
            $response = WepayController::payWePay($request);
            return view("payform", ["checkoutUri" => $response->hosted_checkout->checkout_uri, "methodPay" => "wepay"]);
        } elseif ($request->methodPay == "stripe") {
            //$amount = $request->amount;
            //return view("payform", ["request" => $request, "amount" => $amount, "methodPay" => "stripe"]);
            //return redirect("/stripe");
            return StripeController::index($request);
        } else {
            return "method_not_valid";
        }
    }
}
