<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Company;
use App\Donate;
use App\Payment;
use App\StripeAccountRegister;
use App\StripeCheckout;
use App\SystemOption;
use App\User;
use App\WepayCheckout;
use App\WepayUserRegister;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.templates.index');
    }

    public function companies()
    {
        $companies = Company::paginate(15);
        return view('admin.templates.companies', ['companies' => $companies]);
    }

    public function users()
    {
        $users = User::all();
        return view('admin.templates.users', ["users" => $users]);
    }

    public function payments()
    {
        $payments = Payment::paginate(15);
        return view('admin.templates.payments', ['payments' => $payments]);
    }

    public function donations()
    {
        $donates = Donate::paginate(15);
        return view('admin.templates.donations', ['donates' => $donates]);
    }

    public function docs()
    {
        return view('admin.templates.docs');
    }

    public function checkouts()
    {
        $wepayCheckouts = WepayCheckout::paginate(15);
        return view('admin.templates.checkouts', ['wepayCheckouts' => $wepayCheckouts]);
    }

    public function options()
    {
        $options = SystemOption::paginate(15);
        return view('admin.templates.options', ['options' => $options]);
    }

    public function addOption()
    {
        SystemOption::create([
            'key' => '',
            'value' => '',
        ]);
        return response()->json(['status' => 'complete']);
    }

    public function updateOption(Request $request)
    {
        $option = SystemOption::find($request->id);
        $option[$request->key] = $request->value;
        $option->save();
        return response()->json(['status' => 'complete']);
    }

    public function removeOption(Request $request)
    {
        $option = SystemOption::find($request->id);
        $option->delete();
        return response()->json(['status' => 'complete']);
    }

    public function wepayUsers()
    {
        $users = WepayUserRegister::paginate(20);
        return view('admin.templates.wepay_users', ['users' => $users]);
    }

    public function stripeAccounts()
    {
        $stripes = StripeAccountRegister::paginate(20);
        return view('admin.templates.stripe_accounts', ['stripes' => $stripes]);
    }

    public function stripeCharges()
    {
        $charges = StripeCheckout::paginate(20);
        return view('admin.templates.stripe_charge', ['charges' => $charges]);
    }

    public function companyCategories()
    {
        $categories = Categories::paginate(20);
        return view('admin.templates.categories', ['categories' => $categories]);
    }

    public function addCategory() {
        Categories::create([
            'name' => 'new_cat',
        ]);
        return response()->json(['status' => 'complete']);
    }

    public function removeCategory(Request $request)
    {
        $category = Categories::find($request->id);
        $category->delete();
        return response()->json(['status' => 'complete']);
    }

    public function updateCategory(Request $request)
    {
        $category = Categories::find($request->id);
        $category[$request->key] = $request->value;
        $category->save();
        return response()->json(['status' => 'complete']);
    }

    public function api()
    {
        return view('admin.templates.api');
    }

    public function getUserBalance($id)
    {
        $user = User::find($id);
        if ($user->pay_account_type == "wepay") {
            $info = DB::select("SELECT wa.account_id, wu.access_token FROM users as u JOIN wepay_users_register as wu JOIN wepay_accounts_register as wa
                                WHERE wu.user_local_id = u.id AND wa.owner_user_id = wu.user_id");
            $wepayController = new WepayController();
            $res = $wepayController->paymentAccountInfo($info[0]->account_id, $info[0]->access_token);
            return $res;
        } else {
            $stripeUser = StripeAccountRegister::where('user_local_id', '=', $user->id)->first();
            $stripeController = new StripeController();
            $result = $stripeController->retrieveBalance($stripeUser->secret);
            return response()->json($result);
        }
    }

    /**
     * ajax request. Change company status
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeCompanyStatus(Request $request)
    {
        if (!$request->ajax()) {
            exit;
        }
        $company = Company::find($request->id);
        $company->status = $request->status;
        $company->save();
        return response()->json(["status" => "complete"]);
    }

}
