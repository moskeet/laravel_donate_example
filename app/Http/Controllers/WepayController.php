<?php

namespace App\Http\Controllers;

use App\Company;
use App\Donate;
use App\WepayAccountRegister;
use App\WepayCheckout;
use App\WepayCreditCard;
use App\WepayUserRegister;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

require 'wepay.php';

class WepayController extends Controller
{

    //app
    private $accountId = 850820223;
    private $clientId = 13188;
    private $clientSecret = "023c60b20f";
    private $accessToken = "STAGE_b1f9b5e9460d13cd0912d9271cdef6f355769cf61ec5c1da78e0b07b37de1b06";
    private $appFee = 5;


    public function makePay(Request $request)
    {
        $amount = $request->amount;
        $companyId = $request->company_id;
        //select access_token and account_id
        $res = DB::select("SELECT wu.access_token, wa.account_id from companies as c 
                            JOIN wepay_users_register as wu 
                            JOIN wepay_accounts_register as wa 
                            where c.id = $companyId 
                            and wu.user_local_id = c.user_id
                            and wa.owner_user_id = wu.user_id");
        if (empty($res)) {
            echo "Company not have account. Try later.";
            exit;
        }
        $response = $this->checkoutCreate($amount, $companyId, $res[0]->access_token, $res[0]->account_id, $request->credit_card_id);
        return $response;
    }

    /**
     * Checkout By Credit Card
     * @param $amount
     * @param $companyId
     * @param $access_token
     * @param $account_id
     * @param $credit_card_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkoutCreate($amount, $companyId, $access_token, $account_id, $credit_card_id)
    {
        $client_id = $this->clientId;
        $client_secret = $this->clientSecret;
        \Wepay::useStaging($client_id, $client_secret);
        $wepay = new \WePay($access_token);
        if (Auth::check()) {
            $userId = Auth::user()->id;
        } else {
            $userId = "";
        }
        try {
            $response = $wepay->request('checkout/create', array(
                'account_id' => $account_id,
                'amount' => $amount,
                'short_description' => $_POST['comment'],
                'type' => 'donation',
                'currency' => 'USD',
                'reference_id' => \GuzzleHttp\json_encode(['company_id' => $companyId, 'pay_user_id' => $userId]),
                "fee" => [
                    "app_fee" => $amount / 100 * $this->appFee,
                    "fee_payer" => "payee" //payer
                ],
                'payment_method' => array(
                    'type' => 'credit_card',
                    'credit_card' => array(
                        'id' => $credit_card_id
                    )
                )
            ));
        } catch (\Exception $e) {
            echo 'Exception: ', $e->getMessage(), "\n";
            exit;
        }

        if(isset($_POST['anonymous'] )  && $_POST['anonymous'] == 1){
            $anonymous = 1;
        } else {
            $anonymous = 0;
        }

        $this->donateSuccess($response, $anonymous);

        return response()->json($response);
    }

    public function donateSuccess($response, $anonymous)
    {
        if (isset($response->error)) {
            echo $response->error_description;
        } else {
            $checkout = WepayCheckout::where('checkout_id', '=', $response->checkout_id)->first();
            $res = DB::select("SELECT wu.access_token FROM wepay_accounts_register AS wa JOIN wepay_users_register as wu where wa.account_id = $response->account_id and wa.owner_user_id = wu.user_id");
            $token = $res[0]->access_token;

            if (!isset($checkout)) {
                $wepayController = new WepayController();
                $checkoutInfo = $wepayController->checkoutInfo($response->checkout_id, $token);
                $infoArr = \GuzzleHttp\json_decode($checkoutInfo->reference_id, true);
                $companyId = $infoArr['company_id'];
                $userId = $infoArr['pay_user_id'];


                $wepayCheckout = new WepayCheckout();
                $wepayCheckout->createCheckoutWepay($checkoutInfo, $companyId, $userId);

                $donate = new Donate();
                $donate->createDotate($checkoutInfo, $companyId, $userId, $anonymous);
            }
            $this->balanceReCalculation($response->checkout_id, $token);
        }
    }

    /**
     * recalculation balance company, to view on site
     * @param null $checkoutId
     * @param $token
     */
    public function balanceReCalculation($checkoutId = null, $token)
    {
        $this->checkoutUpdateStatus($checkoutId, $token);
        $checkout = WepayCheckout::where('checkout_id', '=', $checkoutId)->first();
        if ($checkout->state == "released" && $checkout->balance_included == 0) {
            $company = Company::find($checkout->reference_id);
            $this->companyId = $company->id;
            $this->wepayCheckoutId = $checkout->id;
            $this->amount = $company->amount_now + ($checkout->amount - $checkout->amount / 100 * 5); //minus 5%
            DB::transaction(function () {
                DB::table('companies')->where('id', $this->companyId)->update(['amount_now' => $this->amount]);
                DB::table('wepay_checkouts')->where('id', $this->wepayCheckoutId)->update(['balance_included' => 1]);
            });
        }
    }

    public function checkoutUpdateStatus($checkoutId, $token)
    {
        $checkout = WepayCheckout::where('checkout_id', '=', $checkoutId)->first();
        $wepayController = new WepayController();
        $checkoutInfo = $wepayController->checkoutInfo($checkoutId, $token);
        if ($checkout->state != $checkoutInfo->state) {
            $checkout->state = $checkoutInfo->state;
            $checkout->save();
        }
    }


    /*get checkout info*/
    public function checkoutInfo($checkoutId, $access_token)
    {
        $client_id = $this->clientId;
        $client_secret = $this->clientSecret;
        if (\Wepay::$production === null) {
            \Wepay::useStaging($client_id, $client_secret);
        }
        $wepay = new \WePay($access_token);
        $response = $wepay->request('checkout', array(
            'checkout_id' => $checkoutId,
        ));
        return $response;
    }

    /**
     * get checkouts info
     * @param int $start
     * @param int $limit
     * @param string $state
     * @return \StdClass
     */
    public function checkoutsInfo($start = 0, $limit = 50, $state = "")
    {
        $account_id = $this->accountId; // your app's account_id
        $client_id = $this->clientId;
        $client_secret = $this->clientSecret;
        $access_token = $this->accessToken; // your app's access_token
        \Wepay::useStaging($client_id, $client_secret);
        $wepay = new \WePay($access_token);
        $response = $wepay->request('/checkout/find', array(
            'account_id' => $account_id,
            'start' => $start,
            'limit' => $limit,
            'state' => $state,
        ));
        return $response;
    }

    /**
     * create custome account
     */
    public function customAccountCreation($user = null)
    {
        $response = $this->wepayUserRegisterApi($user);
        $this->saveWepayUserLocal($user, $response);
        list($wepay, $response) = $this->createWepayAccountApi($response);
        $this->saveWepayAccountLocal($response);
        $response = $this->createConfirmEmailApi($wepay);
        return $response;
    }

    public function customAccountModify($account_id, $access_token)
    {
        $wepay = new \WePay($access_token);
        $response = $wepay->request('/account/modify', array(
            'account_id' => $account_id,
            'name' => 'Account Name',
            'description' => 'A description for your account.',
            'reference_id' => 'A description for your account.'
        ));
        return array($wepay, $response);
    }


    /**
     * api
     * account object
     */
    public function paymentAccountInfo($accountId, $accessToken)
    {
        $client_id = $this->clientId;
        $client_secret = $this->clientSecret;
        \Wepay::useStaging($client_id, $client_secret);
        $wepay = new \WePay($accessToken);
        $response = $wepay->request('/account', array(
            'account_id' => $accountId,
        ));
        var_dump($response);
    }


    public function paymentAccountsInfo()
    {
        $client_id = $this->clientId;
        $client_secret = $this->clientSecret;
        $access_token = "STAGE_b1f9b5e9460d13cd0912d9271cdef6f355769cf61ec5c1da78e0b07b37de1b06";
        \Wepay::useStaging($client_id, $client_secret);
        $wepay = new \WePay($access_token);
        $response = $wepay->request('/account/find', array());
        var_dump($response);
    }


    public function withdrawals()
    {
        if (!Auth::check()) {
            exit;
        }

        $userPayAccountType = Auth::user()->pay_account_type;
        $userId = Auth::user()->id;
        if ($userPayAccountType == "wepay") {
            $res = DB::select("SELECT wu.access_token, wa.account_id FROM users as u JOIN wepay_users_register as wu JOIN wepay_accounts_register as wa ON wu.user_local_id = $userId AND wu.user_local_id = u.id AND wa.owner_user_id = wu.user_id");
        } else {
            echo "no wepay";
            exit;
        }
        // application settings
        $account_id = $res[0]->account_id;
        $client_id = $this->clientId;
        $client_secret = "$this->clientSecret";
        $access_token = $res[0]->access_token;
        // change to useProduction for live environments
        \Wepay::useStaging($client_id, $client_secret);

        $wepay = new \WePay($access_token);

        try {
            // create the withdrawal
            $response = $wepay->request('account/get_update_uri', array(
                'account_id' => $account_id,
                'redirect_uri' => 'http://donor.loc/',
                'mode' => 'iframe'
            ));
        } catch (\Exception $e) {
            echo 'Exception: ', $e->getMessage(), "\n";
            exit;
        }

        return view('withdrawals', ['response' => $response]);
    }

    /**
     * api
     * @param $user
     * @return \StdClass
     */
    public function wepayUserRegisterApi($user)
    {
        $client_id = $this->clientId;
        $client_secret = $this->clientSecret;
        $access_token = $this->accessToken;
        \Wepay::useStaging($client_id, $client_secret);
        $wepay = new \WePay($access_token);
        $response = $wepay->request('user/register/', array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'email' => $user->email,  //vasadeveloper12@gmail.com
            'scope' => 'manage_accounts,collect_payments,view_user,preapprove_payments,send_money',
            'first_name' => 'Bill',
            'last_name' => 'Clerico',
            'original_ip' => '74.3.224.4',
            'original_device' => 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6;
                             en-US) AppleWebKit/534.13 (KHTML, like Gecko)
                             Chrome/9.0.597.102 Safari/534.13',
            'tos_acceptance_time' => time(),
        ));
        return $response;
    }

    /**
     * api
     * @param $user
     * @param $response
     */
    public function saveWepayUserLocal($user, $response)
    {
        $wepayUser = WepayUserRegister::where('user_id', '=', $response->user_id)->first();
        if (!isset($wepayUser)) {
            WepayUserRegister::create([
                'user_id' => $response->user_id,
                'access_token' => $response->access_token,
                'token_type' => $response->token_type,
                'expires_in' => $response->expires_in,
                'user_local_id' => $user->id,
                'email' => $user->email,
            ]);
        }
    }

    /**
     * api
     * @param $response
     * @return array
     */
    public function createWepayAccountApi($response)
    {
        $access_token = $response->access_token;
        $wepay = new \WePay($access_token);
        $response = $wepay->request('account/create/', array(
            'name' => 'Account Name',
            'description' => 'A description for your account.'
        ));
        return array($wepay, $response);
    }

    /**
     * api
     * @param $response
     */
    public function saveWepayAccountLocal($response)
    {
        $wepayAccount = WepayAccountRegister::where('account_id', '=', $response->account_id)->first();
        if (!isset($wepayAccount)) {
            WepayAccountRegister::create([
                'account_id' => $response->account_id,
                'owner_user_id' => $response->owner_user_id,
                'name' => $response->name,
                'description' => $response->description,
                'type' => $response->type,
                'country' => $response->country,
            ]);
        }
    }

    /**
     * api
     * @param $wepay
     * @return mixed
     */
    public function createConfirmEmailApi($wepay)
    {
        try {
            $response = $wepay->request('user/send_confirmation/', array());
            return $response;
        } catch (\Exception $e) {
            echo 'Exception: ', $e->getMessage(), "\n";
            return "";
        }
    }

    /**
     * api
     * This call allows you to pass credit card information and receive back a credit_card_id
     * @param $creditData
     * @return \StdClass
     */
    public function creditCardCreate(Request $request)
    {
        $client_id = $this->clientId; //The ID for your API application. You can find it on your app dashboard.
        $client_secret = $this->clientSecret;
        $access_token = $this->accessToken;
        \Wepay::useStaging($client_id, $client_secret);
        $wepay = new \WePay($access_token);

        try {
            $response = $wepay->request('/credit_card/create', array(
                'client_id' => $client_id,
                'user_name' => $request->user_name,
                'email' => $request->email,  //vasadeveloper12@gmail.com //The email address of the user who owns the card
                'cc_number' => $request->cc_number,
                'cvv' => $request->cvv,
                'expiration_month' => $request->expiration_month,
                'expiration_year' => $request->expiration_year,
                'address' => [
                    'country' => $request->address['country'],
                    'postal_code' => $request->address['postal_code']
                ],
            ));
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
            //echo 'Exception: ', $e->getMessage(), "\n";
            //exit;
        }

        if (Auth::check() && Auth::user()->pay_account_type == "wepay") {
            if (isset($request->save_credit_card) && $request->save_credit_card == "true")
                WepayCreditCard::create([
                    'client_id' => $client_id,
                    'cc_number' => $request->cc_number,
                    'user_id' => Auth::user()->id,
                    'credit_card_id' => $response->credit_card_id,
                    'state' => $response->state,
                ]);
        }

        return response()->json($response);
    }

}
































