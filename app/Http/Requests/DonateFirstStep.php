<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DonateFirstStep extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'company_id' => 'required|integer',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'zip' => 'required|max:255',
            'comment' => 'required|max:255',
            'country' => 'required|max:255',
        ];
    }

}
