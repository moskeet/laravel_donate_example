<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripeAccountRegister extends Model
{

    protected $table = "stripe_account_register";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'user_local_id',
        'name',
        'country',
        'email',
        'secret',
        'publishable',
    ];
}
