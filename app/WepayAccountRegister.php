<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WepayAccountRegister extends Model
{

    protected $table = "wepay_accounts_register";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'owner_user_id',
        'name',
        'description',
        'type',
        'country',
    ];

}
