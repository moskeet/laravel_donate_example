<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class StripeCustomer extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'customer_id',
        'email',
        'card'
    ];


    public function stripeCustomerSave($email, $id, $card)
    {
        if(Auth::check()){
            $userId = Auth::user()->id;
        } else {
            $userId = null;
        }

        StripeCustomer::create([
            'user_id' => $userId,
            'customer_id' => $id,
            'email' => $email,
            'card' => $card
        ]);
    }
}






