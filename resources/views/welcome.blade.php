<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            /*align-items: center;*/
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
            @endif
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Donations site
        </div>

        <h3>Last Companies</h3>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="/add/new/company" class="btn btn-success">Start Compaign</a>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row">
                @foreach($companies as $company)
                    <div class="col-md-4">
                        <?php
                        $companyImagesArray = json_decode($company->images);
                        if (!isset($companyImagesArray[0])) {
                            $companyImagesArray[0] = "";
                        }
                        ?>
                        <div style="height: 300px; height: 300px; background: url('/thumbnail/{{$companyImagesArray[0]}}') no-repeat center; background-size: cover">
                            {{--<img width="300" src="/thumbnail/{{$companyImagesArray[0]}}" alt="Avatar">--}}
                            <h4 style="color: black; font-weight: bold" class="media-heading">{{$company->name}}</h4>
                            <p style="color: black; font-weight: bold"><a target="_blank"
                                                                          href="/company/{{$company->slug}}">View</a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</body>
</html>
