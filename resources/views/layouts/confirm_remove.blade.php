<!-- Modal confirm remove -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Do you really want to delete?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" onclick="removeConfirm()">Remove</button>
            </div>
        </div>
    </div>
</div>

<script>
    removeId = "";
    function removeItemAjax(id){
        removeId = id;
        $('#myModal').modal('show');
    }

    function removeConfirm() {
        $.ajax({
            url: removeCompanyUrl,
            type: "post",
            data: {_token: window.Laravel.csrfToken, id: removeId},
            success: function (data) {
                $("."+classElement+""+removeId).remove();
                $('#myModal').modal('hide');
            }
        });
    }
</script>