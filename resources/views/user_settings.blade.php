@extends('layouts.app')

<style>
    #app .container {
        width: 100%;
    }
</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h3>User Settings</h3>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bs-example" data-example-id="simple-table">
                                        <p class="error"></p>
                                        <form>
                                            <div class="form-group">
                                                <label for="formGroupExampleInput">Name</label>
                                                <input type="text" value="{{$user->name}}" onchange="changeUserSettings('{{$user->id}}', this, 'name')" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                                            </div>
                                            <div class="form-group">
                                                <label for="formGroupExampleInput">Last Name</label>
                                                <input type="text" value="{{$user->last_name}}" onchange="changeUserSettings('{{$user->id}}', this, 'last_name')" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                                            </div>
                                            <div class="form-group">
                                                <label for="formGroupExampleInput2">Email</label>
                                                <input type="text" value="{{$user->email}}" onchange="changeUserEmail('{{$user->id}}', this)" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                                            </div>
                                        </form>

                                        <p>Saved credit cards</p>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("js")
   <script>
       function changeUserEmail(id, element) {
           var email = $(element).val();
           $.ajax({
               url: "/ajax/change/user/email",
               type: "post",
               data: {_token: window.Laravel.csrfToken, id: id, email: email},
               error: function (data) {
                   console.log(data.responseText);
                   $(".error").text(data.responseText);
               }
           });
       }

       function changeUserSettings(id, element, row) {
           var value = $(element).val();
           $.ajax({
               url: "/ajax/change/user/settings",
               type: "post",
               data: {_token: window.Laravel.csrfToken, id: id, row: row, value: value},
               error: function (data) {
                   console.log(data.responseText);
                   $(".error").text(data.responseText);
               }
           });
       }
   </script>
@endsection



