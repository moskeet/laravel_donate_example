@extends('admin.index')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Donations Table
                    <small>Donations configurations</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <button class="btn btn-default">Add</button>
                <button class="btn btn-info">Edit selected</button>
                <button class="btn btn-danger">Remove selected</button>

                <div class="table-responsive">

                    <table id="wepaycheckouts" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>checkout_id</th>
                            <th>account_id</th>
                            <th>type</th>
                            <th>short_description</th>
                            <th>currency</th>
                            <th>amount</th>
                            <th>state</th>
                            <th>gross</th>
                            <th>payer_email</th>
                            <th>payer_name</th>
                            <th>reference_id</th>
                            <th>user_pay_id</th>
                            <th>balance_included</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>checkout_id</th>
                            <th>account_id</th>
                            <th>type</th>
                            <th>short_description</th>
                            <th>currency</th>
                            <th>amount</th>
                            <th>state</th>
                            <th>gross</th>
                            <th>payer_email</th>
                            <th>payer_name</th>
                            <th>reference_id</th>
                            <th>user_pay_id</th>
                            <th>balance_included</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($wepayCheckouts as $checkout)
                            <tr>
                                <td>{{$checkout->checkout_id}}</td>
                                <td>{{$checkout->account_id}}</td>
                                <td>{{$checkout->type}}</td>
                                <td>{{$checkout->short_description}}</td>
                                <td>{{$checkout->currency}}</td>
                                <td>{{$checkout->amount}}</td>
                                <td>{{$checkout->state}}</td>
                                <td>{{$checkout->gross}}</td>
                                <td>{{$checkout->payer_email}}</td>
                                <td>{{$checkout->payer_name}}</td>
                                <td>{{$checkout->reference_id}}</td>
                                <td>{{$checkout->user_pay_id}}</td>
                                <td>{{$checkout->balance_included}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <?php echo $wepayCheckouts->render(); ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function() {
            $('#wepaycheckouts').DataTable();
        } );
    </script>
@endsection
