@extends('admin.index')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Companies Table
                    <small>Companies configurations</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <button class="btn btn-default">Add</button>
                <button class="btn btn-info">Edit selected</button>
                <button class="btn btn-danger">Remove selected</button>

                <div class="table-responsive">

                    <table id="companies" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>name</th>
                            <th>amount_need</th>
                            <th>amount_now</th>
                            <th>status</th>
                            <th>user_id</th>
                            <th>category_id</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>name</th>
                            <th>amount_need</th>
                            <th>amount_now</th>
                            <th>status</th>
                            <th>user_id</th>
                            <th>category_id</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($companies as $company)
                            <tr>
                                <td>{{$company->name}}</td>
                                <td>{{$company->amount_need}}</td>
                                <td>{{$company->amount_now}}</td>
                                <td>
                                    <select class="company_status" company_id="{{$company->id}}">
                                        <option @if($company->status == "moderate") selected @endif value="moderate">moderate</option>
                                        <option @if($company->status == "enabled") selected @endif value="enabled">enabled</option>
                                        <option @if($company->status == "disabled") selected @endif value="disabled">disabled</option>
                                    </select>
                                </td>
                                <td>{{$company->user_id}}</td>
                                <td>{{$company->category_id}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <?php echo $companies->render(); ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function() {
            $('#companies').DataTable();
        } );

        $( ".company_status" ).change(function() {
            console.log(1)
            companyId = $(this).attr("company_id");
            status = $(this).val();
            $.ajax({
                url: "/admin/ajax/company/change/status",
                type: "post",
                data: {_token: window.Laravel.csrfToken, id: companyId, status: status},
            });
        });

    </script>
@endsection