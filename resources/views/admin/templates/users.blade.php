@extends('admin.index')

@section("css")
@endsection

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users Table <small>Users configurations</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <button class="btn btn-default">Add</button>
                <button class="btn btn-info">Edit selected</button>
                <button class="btn btn-danger">Remove selected</button>

                <div class="table-responsive">

                    <table id="users" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>name</th>
                            <th>email</th>
                            <th>status</th>
                            <th>last_ip</th>
                            <th>system_status</th>
                            <th>country</th>
                            <th>timezone</th>
                            <th>ref</th>
                            <th>pay_account_type</th>
                            <th>balance</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>name</th>
                            <th>email</th>
                            <th>status</th>
                            <th>last_ip</th>
                            <th>system_status</th>
                            <th>country</th>
                            <th>timezone</th>
                            <th>ref</th>
                            <th>pay_account_type</th>
                            <th>balance</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->status}}</td>
                            <td>{{$user->last_ip}}</td>
                            <td>{{$user->system_status}}</td>
                            <td>{{$user->country}}</td>
                            <td>{{$user->timezone}}</td>
                            <td>{{$user->ref}}</td>
                            <td>{{$user->pay_account_type}}</td>
                            <td><a target="_blank" href="/admin/user/balance/{{$user->id}}">view balance</a></td>
                        </tr>
                         @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>

@endsection

@section("js")
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#users').DataTable();
        } );
    </script>
@endsection