@extends('admin.index')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Donations Table
                    <small>Donations configurations</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <button class="btn btn-default">Add</button>
                <button class="btn btn-info">Edit selected</button>
                <button class="btn btn-danger">Remove selected</button>

                <div class="table-responsive">

                    <table id="donations" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>description</th>
                            <th>amount</th>
                            <th>status</th>
                            <th>company_id</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>description</th>
                            <th>amount</th>
                            <th>status</th>
                            <th>company_id</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($donates as $donate)
                            <tr>
                                <td>{{$donate->description}}</td>
                                <td>{{$donate->amount}}</td>
                                <td>{{$donate->status}}</td>
                                <td>{{$donate->company_id}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <?php echo $donates->render(); ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function() {
            $('#donations').DataTable();
        } );
    </script>
@endsection