@extends('admin.index')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Donations Table
                    <small>Donations configurations</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <button class="btn btn-default" onclick="addOption()">Add</button>
                <button class="btn btn-info">Edit selected</button>
                <button class="btn btn-danger">Remove selected</button>

                <div class="table-responsive">

                    <table id="options" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>key</th>
                            <th>value</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>key</th>
                            <th>value</th>
                            <th>action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($options as $option)
                            <tr class="option-{{$option->id}}">
                                <td><input onchange="updateOption('key', '{{$option->id}}', this)" class="options" value="{{$option->key}}"></td>
                                <td><input onchange="updateOption('value', '{{$option->id}}', this)" class="options" value="{{$option->value}}"></td>
                                <td class="removeOption"><span onclick="removeOption('{{$option->id}}')" style="cursor: pointer;" class="glyphicon glyphicon-remove"></span></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <?php echo $options->render(); ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function () {
            $('#options').DataTable();
        });

        function addOption() {
            $.ajax({
                url: "/admin/add/option",
                type: "post",
                data: {_token: window.Laravel.csrfToken},
                success: function (data) {
                    location.reload();
                }
            });
        }

        function updateOption(key, optionId, element) {
            $.ajax({
                url: "/admin/update/option",
                type: "post",
                data: {_token: window.Laravel.csrfToken, id: optionId, key: key, value: $(element).val()}
            });
        }

        function removeOption(optionId) {
            $.ajax({
                url: "/admin/remove/option",
                type: "post",
                data: {_token: window.Laravel.csrfToken, id: optionId},
                success: function (data) {
                    $(".option-"+optionId).remove();
                }
            });
        }
    </script>
@endsection