<form action="/stripe/charge" method="POST">
    {!! csrf_field() !!}
    <input type="hidden" value="{{$amount * 100}}" name="amount">
    <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="pk_test_eEiW9mXcLEd1RmjAX0iwshHO"
            data-amount="{{$amount * 100}}"
            data-name="Demo Site"
            data-description="Widget"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
            data-locale="auto">
    </script>
</form>