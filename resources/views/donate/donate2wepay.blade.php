@extends('layouts.app')

<style>
    #app .container {
        width: 100%;
    }
    /* #wepay_checkout_iframe {
         width: 100% !important;
     }
     */
</style>
<link href="{{asset('css/social-buttons.css')}}" rel="stylesheet">

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h3>Donate for company</h3>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">


                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">
                                                            Payment Details
                                                        </h3>

                                                    </div>
                                                    <div class="panel-body">

                                                        @if($errors->any())
                                                            <h4>{{$errors->first()}}</h4>
                                                        @endif

                                                        @if(isset($creditCards) && !empty($creditCards))
                                                            <p>Select card</p>
                                                            @foreach($creditCards as $card)
                                                                <label>{{$card->cc_number}}</label>
                                                                <input type="radio" name="selected_card"
                                                                       value="{{$card->credit_card_id}}">
                                                                <br>
                                                            @endforeach
                                                            <hr>
                                                        @endif

                                                        <p class="error_place" style="color: red"></p>
                                                            <label>New Card</label>
                                                            <input type="radio" checked name="selected_card" value="new_card">
                                                        <form id="payform" role="form" method="post" action="/stripe/pay">
                                                            <input type="hidden" id="amount" name="amount" value="{{$request->amount}}">
                                                            <input type="hidden" id="first_name" required name="first_name" value="{{$request->first_name}}">
                                                            <input type="hidden" id="last_name" required name="last_name" value="{{$request->last_name}}">
                                                            <input type="hidden" id="email" required name="email" value="{{$request->email}}">
                                                            <input type="hidden" id="zip" required name="zip" value="{{$request->zip}}">
                                                            <input type="hidden" id="comment" required name="comment" value="{{$request->comment}}">
                                                            <input type="hidden" id="country" required name="country" value="{{$request->country}}">
                                                            <input type="hidden" id="company_id" required name="company_id" value="{{$request->company_id}}">
                                                            <input type="hidden" id="comment" required name="comment" value="{{$request->comment}}">
                                                            <input type="hidden" id="donationAnonymous" required name="donationAnonymous" value="{{$request->donationAnonymous}}">
                                                            {{ csrf_field() }}
                                                            <div class="form-group">
                                                                <label for="cardNumber">
                                                                    CARD NUMBER</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" id="cardNumber" name="card" placeholder="Valid Card Number"
                                                                           required autofocus />
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-7 col-md-7">
                                                                    <div class="form-group">
                                                                        <label for="expityMonth">
                                                                            EXPIRY DATE</label>
                                                                        <div class="col-xs-6 col-lg-6 pl-ziro">
                                                                            <input type="text" class="form-control" name="expityMonth" id="expityMonth" placeholder="MM" required />
                                                                        </div>
                                                                        <div class="col-xs-6 col-lg-6 pl-ziro">
                                                                            <input type="text" class="form-control" name="expityYear" id="expityYear" placeholder="YY" required /></div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-5 col-md-5 pull-right">
                                                                    <div class="form-group">
                                                                        <label for="cvCode">
                                                                            CV CODE</label>
                                                                        <input type="password" class="form-control" name="ccv" id="cvCode" placeholder="CV" required />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div style="padding-left: 20px">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="billing_address" name="billing_address" placeholder="Billing Address"
                                                                               required autofocus />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="city" name="city" placeholder="City"
                                                                               required autofocus />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="region" name="region" placeholder="Region"
                                                                               required autofocus />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode"
                                                                               required autofocus />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="checkbox pull-right" @if(!\Illuminate\Support\Facades\Auth::check())style="display: none"@endif>
                                                                <label>
                                                                    <input id="save_credit_card" type="checkbox" />
                                                                    Remember credit card
                                                                </label>
                                                            </div>


                                                            <br/>
                                                            <button id="submit_pay" type="submit" href="" class="btn btn-success btn-lg btn-block" role="button">Pay</button>
                                                        </form>



                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")

    <script>

        $("#submit_pay").on("click", function () {
            value = $("input[name='selected_card']:checked").val();
            if(value != "new_card"){
                event.preventDefault();
                $("#payform").submit();
            }
        });

        $( "#payform" ).submit(function( event ) {
            event.preventDefault();
            cvCode = $('#cvCode').val();
            expityMonth = $('#expityMonth').val();
            expityYear = $('#expityYear').val();
            cardNumber = $('#cardNumber').val();
            billing_address = $('#billing_address').val();
            city = $('#city').val();
            country = $('#country').val();
            email = $('#email').val();
            region = $('#region').val();
            postcode = $('#postcode').val();
            userName = $('#first_name').val() + " " + $('#last_name').val();
            saveCreditCard = $('#save_credit_card').prop('checked');
            clientId = 13188;
            callApiCardCreate();

        });

        /**
         * create card api call
         */
        function callApiCardCreate() {

            value = $("input[name='selected_card']:checked").val();

            if(value == "new_card") {
                objectPay = {
                    _token: window.Laravel.csrfToken,
                    client_id: clientId,
                    user_name: userName,
                    email: email,
                    cc_number: cardNumber,
                    cvv: cvCode,
                    expiration_month: expityMonth,
                    expiration_year: expityYear,
                    save_credit_card: saveCreditCard,
                    address: {country: country, postal_code: postcode}
                };

                $.ajax({
                    url: "/ajax/wepay/credit_card/create",
                    type: "post",
                    dataType: 'json',
                    data:  objectPay ,
                    success: function (data) {
                        callApiCheckoutCreate(data.credit_card_id);
                    },
                    error: function (data) {
                        console.log(data.responseJSON.error);
                        $(".error_place").text(data.responseJSON.error);
                    }
                });

            } else {
                /*Use saved card*/
                callApiCheckoutCreate(value);
            }
        }

        function callApiCheckoutCreate(creditCardId) {

            amount = $('#amount').val();
            company_id = $('#company_id').val();
            comment = $('#comment').val();
            donationAnonymous = $('#donationAnonymous').val();
            credit_card_id = creditCardId;

            objectPay = {
                _token: window.Laravel.csrfToken,
                company_id: company_id,
                amount: amount,
                comment: comment,
                credit_card_id: credit_card_id,
                anonymous: donationAnonymous
            };

            $.ajax({
                url: "/ajax/wepay/custome/checkout/create",
                type: "post",
                dataType: 'json',
                data:  objectPay ,
                success: function (data) {
                    window.location.href ="/success/page/";
                }
            });
        }

    </script>

@endsection


