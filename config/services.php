<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1356054664484756',
        'client_secret' => 'a76503d990aa69661fda2fb9c00103de',
        'redirect' => 'http://donor.loc/login/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'GhIfbjjWpdZlgffykZqfpji79',
        'client_secret' => 'aRc5Ocl5dWvCQzO1JxTBZCCt6xc4HW1gmr6E1lPuhqMCivMvw4',
        'redirect' => 'http://donor.loc/login/twitter/callback',
    ],
    'google' => [
        'client_id' => '938388080361-tfsra02k2uqfh4vp389ffmjk05b2skr8.apps.googleusercontent.com',
        'client_secret' => 'IkAlCW9e0Vt9K8cf77d6Tgo6',
        'redirect' => 'http://donor.com/callback/google',
    ],

];
