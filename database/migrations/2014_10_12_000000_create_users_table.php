<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('last_name', 255);
            $table->string('email', 50)->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('status', 50)->default("active");
            $table->string('system_status', 50)->nullable();
            $table->string('last_ip', 50)->nullable();
            $table->string('country', 50)->nullable();
            $table->string('timezone', 50)->nullable();
            $table->string('ref', 50)->nullable();
            $table->string('pay_account_type')->nullable();
            $table->boolean('is_admin_user')->default(0);
            $table->boolean('newsletters')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
