<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWepayCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wepay_checkouts', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('checkout_id');
            $table->bigInteger('account_id');
            $table->string('type')->nullable();
            $table->string('short_description')->nullable();
            $table->string('currency')->nullable();
            $table->string('amount')->nullable();
            $table->string('state')->nullable();
            $table->string('gross')->nullable();
            $table->string('payer_email')->nullable();
            $table->string('payer_name')->nullable();
            $table->string('reference_id')->nullable();
            $table->string('user_pay_id')->nullable();
            $table->boolean('balance_included')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wepay_checkouts');
    }
}
