<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeAccountsRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_account_register', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_id');
            $table->integer('user_local_id')->unsigned()->nullable();
            $table->foreign('user_local_id')->references('id')->on('users')->onDelete('set null');
            $table->string('name')->nullable();
            $table->string('country');
            $table->string('email');
            $table->string('secret');
            $table->string('publishable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_account_register');
    }
}
