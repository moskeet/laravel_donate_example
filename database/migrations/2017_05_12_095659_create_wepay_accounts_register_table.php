<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWepayAccountsRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wepay_accounts_register', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('account_id');
            $table->bigInteger('owner_user_id');
            $table->string('name');
            $table->string('description');
            $table->string('type');
            $table->string('country');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wepay_accounts_register');
    }
}
