<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 19; $i++){
            DB::table('users')->insert([
                'name' => str_random(10),
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('secret'),
                'status' => "Hello my friends",
                'system_status' => "active",
                'last_ip' => "1.1.1.1",
                'country' => "UK",
                'timezone' => "+3",
                'ref' => "",
            ]);
        }

        DB::table('users')->insert([
            'name' => "tester",
            'email' => 'test@gmail.com',
            'password' => bcrypt('zaq123'),
            'status' => "Hello my friends",
            'system_status' => "active",
            'last_ip' => "1.1.1.1",
            'country' => "UK",
            'timezone' => "+3",
            'ref' => "",
        ]);
    }
}
