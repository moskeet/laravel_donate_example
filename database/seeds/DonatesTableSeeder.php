<?php

use Illuminate\Database\Seeder;

class DonatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++){
            DB::table('donates')->insert([
                'description' => str_random(10),
                'amount' => rand(100, 300),
                'status' => "complete",
                'company_id' => rand(1,19),
            ]);
        }
    }
}

